/* pathalias -- by steve bellovin, as told to peter honeyman */

#ifdef MAIN
#ifndef lint
static char	*c_sccsid = "@(#)config.h	9.5 91/06/11";
#endif /*lint*/
#endif /*MAIN*/

/* the usual case: unix */
#define	NULL_DEVICE	"/dev/null"
#define	OK		0
#define	ERROR		1
#define	SEVERE_ERROR	(-1)
#define STDIO_H		<stdio.h>
#define CTYPE_H		<ctype.h>

#include <stdlib.h>
#include <string.h>

#ifdef STRCHR
#define index strchr
#define rindex strrchr
#else
#define strchr index
#define strrchr rindex
#endif

#ifdef BZERO
#define strclear(s, n)	((void) bzero((s), (n)))
#else /*!BZERO*/

#ifdef MEMSET
extern char	*memset();
#define strclear(s, n)	((void) memset((s), 0, (n)))
#else /*!MEMSET*/
extern void	strclear();
#endif /*MEMSET*/

#endif /*BZERO*/

extern char	*strcpy(), *index(), *rindex();

#ifndef STATIC

#ifdef DEBUG
#define STATIC extern
#else /*DEBUG*/
#define STATIC static
#endif /*DEBUG*/

#endif /*STATIC*/
