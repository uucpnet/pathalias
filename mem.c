#ifndef lint
static char	*sccsid = "@(#)mem.c	9.6 92/08/25";
#endif

#include "def.h"

/* exports */
long Ncount;
extern void freelink(), freetable();

/* imports */
extern char *Netchars;
extern int Vflag;
extern void die();

/* privates */
STATIC void nomem();
static link *Lcache;

link	*
newlink()
{	register link *rval;

	if (Lcache) {
	 	rval = Lcache;
		Lcache = Lcache->l_next;
		strclear((char *) rval, sizeof(link));
	} else if ((rval = (link * ) calloc(1, sizeof(link))) == 0)
		nomem();
	return rval;
}

/* caution: this destroys the contents of l_next */
void
freelink(l)
	link *l;
{
	l->l_next = Lcache;
	Lcache = l;
}

node	*
newnode()
{	register node *rval;

	if ((rval = (node * ) calloc(1, sizeof(node))) == 0)
		nomem();
	Ncount++;
	return rval;
}

dom *
newdom()
{       register dom *rval;

	if ((rval = (dom * ) calloc(1, sizeof(dom))) == 0)
		nomem();

	return rval;
}


char	*
strsave(s)
	char *s;
{	register char *r;

	if ((r = malloc((unsigned) strlen(s) + 1)) == 0)
		nomem();
	(void) strcpy(r, s);
	return r;
}

#ifndef strclear
void
strclear(str, len)
	register char *str;
	register long len;
{
	while (--len >= 0)
		*str++ = 0;
}
#endif /*strclear*/

node	**
newtable(size)
	long size;
{	register node **rval;

	if ((rval = (node **) calloc(1, (unsigned int) size * sizeof(node *))) == 0) 
		nomem();
	return rval;
}

void
freetable(t, size)
	node **t;
	long size;
{
	free((char *) t);
}

STATIC void
nomem()
{
	die("out of memory");
}
